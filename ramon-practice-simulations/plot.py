import numpy
import matplotlib.pyplot

f = 'ring3-output.log'
q = 'potential_energy'
#add the file name above and quantity

data = numpy.genfromtxt(f, skip_header=True)
print('average potential energy',numpy.mean(data[:,1]))
print('average temperature', numpy.mean(data[:,2]))

fig = matplotlib.pyplot.figure(figsize=(8,4.4), dpi=140)
axes1 = fig.add_subplot(1, 2.2, 1)
axes2 = fig.add_subplot(1, 2.2, 2)

axes1.plot(data[:,0], data[:,1])
axes1.set_xlabel('time step')
axes1.set_ylabel(q)

axes2.plot(data[:,0], data[:,2])
axes2.set_xlabel('time step')
axes2.set_ylabel('temperature')
        
matplotlib.pyplot.show()
