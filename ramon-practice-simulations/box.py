import mbuild as mb
benzene = mb.load('benzene.mol2')
benzene.label_rigid_bodies()
filled_box = mb.packing.fill_box(benzene, box=[10,10,10], n_compounds=50, fix_orientation=True)
filled_box.save('benzene_box.hoomdxml', overwrite=True)
