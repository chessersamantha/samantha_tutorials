import hoomd
import hoomd.md

hoomd.context.initialize("")

uc = hoomd.lattice.unitcell(N = 1,
                            a1 = [5, 0, 0],
                            a2 = [0, 5, 0],
                            a3 = [0, 0, 6],
                            dimensions = 3,
                            position = [[0,0,0]], 
                            type_name = ['R'],
                            mass = [1.0],
                            moment_inertia = [21, 28, 49])

system = hoomd.init.create_lattice(unitcell=uc, n=[2,4,2])

system.particles.types.add('A')

rigid = hoomd.md.constrain.rigid()

rigid.set_param('R',
                types=['A']*6,
                positions= [(-1, 0.5, 0), (0, 1, 0), (1, 0.5, 0),
                            (-1, -0.5, 0), (0, -1, 0), (1, -0.5, 0)])
rigid.create_bodies()

nl = hoomd.md.nlist.cell()

lj = hoomd.md.pair.lj(r_cut=2.5, nlist=nl)

lj.set_params(mode='xplor')

lj.pair_coeff.set(['R', 'A'], ['R', 'A'], epsilon=1.0, sigma=1.0)

hoomd.md.integrate.mode_standard(dt=0.005)

rigid = hoomd.group.rigid_center()

hoomd.md.integrate.langevin(group=rigid, kT=1.0, seed=73)

hoomd.analyze.log(filename="log-output.log",
                    quantities=['potential_energy',
                                'translational_kinetic_energy',
                                'rotational_kinetic_energy'],
                    period=100,
                    overwrite=True)

hoomd.dump.gsd('trajectory.gsd',
                1e4,
                hoomd.group.all(),
                overwrite=True)

hoomd.run(4e5)
